HTML=html
BOOK=cgdoc

all:
	dblatex $(BOOK).xml

clean:
	rm -rvf $(BOOK).pdf
